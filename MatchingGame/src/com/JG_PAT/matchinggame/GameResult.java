package com.JG_PAT.matchinggame;

import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.TextView;

public class GameResult extends Activity {
	@Override
	public void onCreate(Bundle savedInstanceState){
		super.onCreate(savedInstanceState);
		setContentView(R.layout.gameresult_screen);
		
		TextView tv_gameresult = (TextView)findViewById(R.id.tv_gameresult);
	
		String temp;
		int time;
		if(GamePlay.win == true)
		{
			temp = "You win :)";
			//int time = Integer.parseInt(GamePlay.setting.getString("high_score", null));
			String temp_time = MainActivity.setting.getString("high_score", null);
			if(temp_time == null)
			{
				time = 0;
			}
			else time = Integer.parseInt(temp_time);
			if(GamePlay.time < time)
			{
				SharedPreferences.Editor editor = MainActivity.setting.edit();
				editor.putString("high_score", String.valueOf(GamePlay.time));
				editor.commit();
				
				Button con = (Button)findViewById(R.id.btn_con_update);
			    con.setOnClickListener(new OnClickListener(){
			       	
				@Override
				public void onClick(View v) {
					// TODO Auto-generated method stub
					Intent intent = new Intent(v.getContext(), HighScoreUpdate.class);
					v.getContext().startActivity(intent);
				}
				        
				});	
			}
			else
			{
				 Button con = (Button)findViewById(R.id.btn_con_update);
			     con.setOnClickListener(new OnClickListener(){
			       	
				 @Override
				 public void onClick(View v) {
				 	// TODO Auto-generated method stub
				 	Intent intent = new Intent(v.getContext(), HighScore.class);
					v.getContext().startActivity(intent);
				 }
				        
				 });	
			}
		}
		else
		{
			temp = "You lose :(";
			 Button con = (Button)findViewById(R.id.btn_con_update);
		     con.setOnClickListener(new OnClickListener(){
		       	
			 @Override
			 public void onClick(View v) {
			 	// TODO Auto-generated method stub
			 	Intent intent = new Intent(v.getContext(), HighScore.class);
				v.getContext().startActivity(intent);
			 }
			        
			 });	
		}
		tv_gameresult.setText(temp);
		
				
		
	}
	@Override
	public void onBackPressed() {
	    // Do Here what ever you want do on back press;
	}
}
