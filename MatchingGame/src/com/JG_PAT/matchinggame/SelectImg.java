package com.JG_PAT.matchinggame;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;

public class SelectImg extends Activity {
	@Override
	public void onCreate(Bundle savedInstanceState){
		super.onCreate(savedInstanceState);
		setContentView(R.layout.selectimg_screen);
		
		 Button ok = (Button)findViewById(R.id.btn_ok);
	     ok.setOnClickListener(new OnClickListener(){
	       	
	     @Override
		public void onClick(View v) {
			// TODO Auto-generated method stub
			Intent intent = new Intent(v.getContext(), GamePlay.class);
			v.getContext().startActivity(intent);
		}
	        	
	    });
	     
	     
	}
	 @Override
	    public void onBackPressed()
	    {
	    	Intent intent = new Intent(this, MainActivity.class);
	        startActivity(intent);
	    }
}


private Cursor cursor;
//String imagePath;
/*
* Column index for the Thumbnails Image IDs.
*/
//   private int columnIndex;
/*
@Override
public void onCreate(Bundle savedInstanceState) {
  super.onCreate(savedInstanceState);
  setContentView(R.layout.help_screen);

  // Set up an array of the Thumbnail Image ID column we want
  String[] projection = {MediaStore.Images.Thumbnails._ID};
  // Create the cursor pointing to the SDCard
  cursor = managedQuery( MediaStore.Images.Thumbnails.EXTERNAL_CONTENT_URI,
          projection, // Which columns to return
          null,       // Return all rows
          null,
          MediaStore.Images.Thumbnails.IMAGE_ID);
  // Get the column index of the Thumbnails Image ID
  columnIndex = cursor.getColumnIndexOrThrow(MediaStore.Images.Thumbnails._ID);

  GridView sdcardImages = (GridView) findViewById(R.id.sdcard);
  sdcardImages.setAdapter(new ImageAdapter(this));

  // Set up a click listener
  sdcardImages.setOnItemClickListener(new OnItemClickListener() {
      public void onItemClick(AdapterView parent, View v, int position, long id) {
          // Get the data location of the image
          String[] projection = {MediaStore.Images.Media.DATA};
          cursor = managedQuery( MediaStore.Images.Media.EXTERNAL_CONTENT_URI,
                  projection, // Which columns to return
                  null,       // Return all rows
                  null,
                  null);
          columnIndex = cursor.getColumnIndexOrThrow(MediaStore.Images.Media.DATA);
          cursor.moveToPosition(position);
          // Get image filename
          imagePath = cursor.getString(columnIndex);
          
          // Use this path to do further processing, i.e. full screen display
      }
  
  
  });
  

}
*/


/**
* Adapter for our image files.
*/
/*
private class ImageAdapter extends BaseAdapter {

  private Context context;

  public ImageAdapter(Context localContext) {
      context = localContext;
  }

  public int getCount() {
      return cursor.getCount();
  }
  public Object getItem(int position) {
      return position;
  }
  public long getItemId(int position) {
      return position;
  }
  public View getView(int position, View convertView, ViewGroup parent) {
      ImageView picturesView;
      if (convertView == null) {
          picturesView = new ImageView(context);
          // Move cursor to current position
          cursor.moveToPosition(position);
          // Get the current value for the requested column
          int imageID = cursor.getInt(columnIndex);
          // Set the content of the image based on the provided URI
          picturesView.setImageURI(Uri.withAppendedPath(
                  MediaStore.Images.Thumbnails.EXTERNAL_CONTENT_URI, "" + imageID));
          picturesView.setScaleType(ImageView.ScaleType.FIT_CENTER);
          picturesView.setPadding(8, 8, 8, 8);
          picturesView.setLayoutParams(new GridView.LayoutParams(100, 100));
      }
      else {
          picturesView = (ImageView)convertView;
      }
      return picturesView;
  }
}*/