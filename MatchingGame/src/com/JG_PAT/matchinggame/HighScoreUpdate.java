package com.JG_PAT.matchinggame;

import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;

public class HighScoreUpdate extends Activity {
	@Override
	public void onCreate(Bundle savedInstanceState){
		super.onCreate(savedInstanceState);
		setContentView(R.layout.highscoreupdate_screen); 
		
		final EditText et_name = (EditText)findViewById(R.id.et_highscore_update);
		
		Button con = (Button)findViewById(R.id.btn_con_main);
		con.setOnClickListener(new OnClickListener(){
		@Override
		public void onClick(View v) {
			// TODO Auto-generated method stub
			
			String name = et_name.getText().toString();
			
			SharedPreferences.Editor editor = MainActivity.setting.edit();
			editor.putString("name", name);
			editor.commit();
			 
		 	Intent intent = new Intent(v.getContext(), MainActivity.class);
		 	v.getContext().startActivity(intent);
		}
		});
	}
	 @Override
	    public void onBackPressed()
	    {
	    	
	    }
}
