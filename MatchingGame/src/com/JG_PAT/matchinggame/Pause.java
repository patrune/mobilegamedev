package com.JG_PAT.matchinggame;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;

public class Pause extends Activity {
	@Override
	public void onCreate(Bundle savedInstanceState){
		super.onCreate(savedInstanceState);
		setContentView(R.layout.pause_screen);
		
		
		 Button main = (Button)findViewById(R.id.btn_mainmenu);
	     main.setOnClickListener(new OnClickListener(){
	      	
		 @Override
		 public void onClick(View v) {
		 	// TODO Auto-generated method stub
		 	Intent intent = new Intent(v.getContext(), MainActivity.class);
			v.getContext().startActivity(intent);
		 }
		        
		 });	
	}
}
